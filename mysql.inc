<?php

include_once("dbinfo.inc");

function call_procedure_get_json($procname, $param_array) {
	header('Content-Type: text/html; charset=utf-8');
	$connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
	mysqli_set_charset($connection, "utf8");
	$query = 'CALL ' . $procname . '(';
	for($i = 0; $i < count($param_array); ++$i) {
    	$query .= '\'' . $param_array[$i] . '\'';
    	if ($i != count($param_array) - 1) {
    		$query .= ',';
    	}
	}
	$query .= ");";
	
	if (mysqli_connect_errno()) { 
		echo '{"error":"500"}';
	}

	$database = mysqli_select_db($connection, DB_DATABASE);
	$result =  mysqli_query($connection, $query);

	$rows = array();
	while($query_data = mysqli_fetch_row($result)) {
		$rows[] = $query_data;
	}
	return json_encode($rows);
}
